import json
import time

from bs4 import BeautifulSoup
from selenium import webdriver


ITEMS_PER_PAGE = 25
IS_PAGE_LIST_ALIVE = True
final_med_list = []
REQUEST_DELAY = .3

url = "https://www.dgda.gov.bd/index.php/manufacturers/allopathic"
browser = webdriver.Chrome("webdriver/chromedriver")
browser.get(url)


def append_each_page_contents():
    soup = BeautifulSoup(browser.page_source, "html.parser")
    med_list = soup.find_all(True, {'class': ['odd', 'even']})
    for index in range(ITEMS_PER_PAGE):
        each_med = {
            "sl": med_list[index].find(class_="sl center").get_text(),
            "manufacturer": med_list[index].find(class_="Name of the Pharmaceutical").get_text(),
            "brand_name": med_list[index].find(class_="Brand Name").get_text(),
            "generic_name": med_list[index].find(class_="Generic Name").get_text(),
            "strength": med_list[index].find(class_="Strength").get_text(),
            "dosages": med_list[index].find(class_="Dosages").get_text(),
            "price": med_list[index].find(class_="PRICE").get_text(),
            "use_for": med_list[index].find(class_="Use for").get_text(),
            "dar": med_list[index].find(class_="DAR").get_text()

        }
        final_med_list.append(each_med)


while IS_PAGE_LIST_ALIVE:
    try:
        append_each_page_contents()
        print(len(final_med_list))
        next_link = browser.find_element_by_xpath("""//*[@id="gridData_next"]""")
        time.sleep(REQUEST_DELAY)
        print("next page")
        next_link.click()

    except:
        IS_PAGE_LIST_ALIVE = False

with open('data.json', 'w', encoding='utf-8') as f:
    json.dump(final_med_list, f, ensure_ascii=False, indent=2)
